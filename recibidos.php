<?php

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL            => "https://api.ultramsg.com/instance2582/messages?token=8zeffs925f6vech1&page=1&limit=10&status=all&sort=desc&id={ID}&referenceId=&from={FROM}&to={TO}&ack={ACK}",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING       => "",
    CURLOPT_MAXREDIRS      => 10,
    CURLOPT_TIMEOUT        => 30,
    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST  => "GET",
    CURLOPT_HTTPHEADER     => array(
        "content-type: application/x-www-form-urlencoded",
    ),
));

$response = curl_exec($curl);
$err      = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}
